<?php
require_once("session.php");

require_once './../../vendor/autoload.php';

use App\User\User;
use App\Photo\Photo;
use \PDO;

$auth_user = new User();
$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id" => $user_id));
$userRow = $stmt->fetch(PDO::FETCH_ASSOC);


$user_photo = new Photo();
$photo = $user_photo->profile();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" media="screen"/>
        <link rel="stylesheet" href="../../assets/css/app.css" type="text/css"  />
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"  />
        <link rel="stylesheet" href="../../assets/css/picedit.min.css" type="text/css"  />
        <link href='https://fonts.googleapis.com/css?family=Hind+Siliguri:400,300,500,600,700&subset=bengali,latin' rel='stylesheet' type='text/css'>
            <script type="text/javascript" src="../../assets/js/jquery-1.11.3-jquery.min.js"></script>
            <title>welcome - <?php print($userRow['email']); ?></title>
    </head>

    <body>


        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="example">Debug & Die</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-user"></span>&nbsp;স্বাগতম ' <?php echo $userRow['email']; ?>&nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;View Profile</a></li>
                                <li><a href="logout.php?logout=true"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="clearfix"></div>

        <div class="container-fluid" style="margin-top:80px;">

            <div class="container">

                <h3>

                    <a href="home.php"><span class="glyphicon glyphicon-home"></span> Home</a>
                    <a href="profile.php"><span class="glyphicon glyphicon-user"></span> Profile</a>
                </h3>
                

                <div class="img-responsive">
                    <?php
                    $srl = 1;
                    foreach ($photo as $pic) {
                        ?>
                        <h3 class="text-info">Existing Picture</h3>
                        <img src="upload/<?php echo $pic['profile_pic']; ?> " width="200px" height="200px"/>

                    </div>

                    <div style=" display: table;">

                        <form action="update.php" method="post" enctype="multipart/form-data" id="xid">
                            <div class="form-group">
                                <br></br>
                                <label>Upload New Photo:</label>
                                <br>
                                    <input type="hidden" name="id" value="<?php echo $pic['id']; ?>" />
                                    <input  class="form-control"  type="file" name="profile_pic" id="thebox"/>  
                            </div>

                            <div style="margin-top:30px;">
                                <button class="btn btn-primary" name="submit" type="submit">Update</button>
                            </div>

                        </form>
                        <div class=""></div>
                        <form action="delete.php" method="POST">
                            <input  type="hidden" name="id" value="<?php echo $pic['id']; ?>"/><br>
                            <button class="btn btn-danger delete" type="submit" >Delete</button>
                        </form>
                        <br>
                        <a href="" class="pull-left "><button class="btn btn-danger" onclick="window.history.go(-1)">Back</button></a>
                    </div>
                    <br/>
                    <?php
                }
                ?>
            </div>

        </div>




        <script src="../../assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="./../../assets/js/picedit.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $('#thebox').picEdit();
            });
        </script>
        <script>
            $('.delete').bind('click',function(e){
                var dlt = confirm("Are you sure to Delete Profile Picture");
                if(!dlt){
                    e.preventDefault();
                }
            });
            $('#msg').fadeOut(5000);
        </script>

    </body>
</html>