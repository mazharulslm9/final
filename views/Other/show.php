<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'others' . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "startup.php");

use App\Other\Other;
use App\Utility\Utility;

$book = new Other();

$books = $book->show($_GET['id']);
$mm = explode(",", $books['languages']);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Other's</title>
        <link rel="stylesheet" href="../../assets/css/bootstrap.min.css" type="text/css"  />
        <link rel="stylesheet" href="../../assets/css/font-awesome.min.css" type="text/css"  />
        <link rel="stylesheet" href="../../assets/css/linecons.css" type="text/css"  />
        <link rel="stylesheet" href="../../assets/css/normalize.css" type="text/css"  />
        <link rel="stylesheet" href="../../assets/css/style_1.css" type="text/css"  />


        <link href='http://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:200,400,300,500,600' rel='stylesheet' type='text/css'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="./../../assets/js/bootstrap.min.js"></script>


    </head>

    <body>
        <!-- Other's Section -->
        <div class="col-md-offset-4 col-md-4 ">
            <div class="hs-content teaching-section">

                <i style="font-size: 30px;padding: 5px;margin: 10px" class="fa fa-language pull-right"></i>
                <div class="hs-inner">

                    <span class="before-title"></span>
                    <h2>VIEW OTHER'S INFORMATION</h2>
                    <div class="teaching-wrapper">
                        <ul class="teaching">
                            <li class="time-label">
                                <span class="content-title">LANGUAGES</span>
                            </li>
                            <li>

                                <div class="timeline-item">
                                    <h3 class="timeline-header">Your Selected Languages</h3>
                                    <div class="timeline-body">

                                        <input 
                                            type="hidden" 
                                            class="form-control" 
                                            name ="id"
                                            value="<?php echo $books['id'] ?>"
                                            />
                                        <label>First Language:</label>
                                        <input readonly="" class="form-control" type="text" value="<?php
                                        if (isset($mm[0])) {
                                            echo $mm[0];
                                        } else {
                                            echo "";
                                        }
                                        ?>" name="languages[]" placeholder="Enter your first language" />
                                        <label>Second Language:</label>
                                        <input readonly="" class="form-control" type="text" value="<?php
                                        if (isset($mm[1])) {
                                            echo $mm[1];
                                        } else {
                                            echo "";
                                        }
                                        ?>" name="languages[]" placeholder="Enter your first language" />
                                        <label>Third Language:</label>
                                        <input readonly="" class="form-control" type="text" value="<?php
                                        if (isset($mm[2])) {
                                            echo $mm[2];
                                        } else {
                                            echo "";
                                        }
                                        ?>" name="languages[]" placeholder="Enter your first language" />
                                    </div>
                                </div>
                            </li>
                            <li class="time-label">
                                <span class="content-title">REFERENCE</span>
                            </li>
                            <li>

                                <div class="timeline-item">
                                    <h3 class="timeline-header">REFERENCE</h3>
                                    <div class="timeline-body">
                                        <label class="control-label">Reference:</label>
                                        <textarea readonly="" class="form-control" cols="60"  name="reference"><?php echo $books['reference']; ?></textarea> <br />



                                    </div>

                                </div>
                            </li>
                            <li>
                                <a href="" class="pull-right "><button class="btn btn-danger" onclick="window.history.go(-1)">Back</button></a>

                            </li>
                        </ul>

                    </div>
                </div>

            </div>

            <!-- End Other's Section -->
        </div>
    </body>
</html>

