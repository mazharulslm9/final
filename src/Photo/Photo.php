<?php

namespace App\Photo;

use App\Utility\Utility;

/**
 * Description of Photo
 *
 * @author Web App Develop PHP
 */
class Photo {

    public $user_id = "";
    public $profile_pic = "";

    public function __construct($data = false, $file = false) {

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("resume") or die("Cannot select database.");


        if (is_array($data) && array_key_exists("id", $data) && !empty($data['id'])) {
            $this->user_id = $data['id'];
        }
        //$this->name = $data['name'];
        $this->profile_pic = $file['profile_pic'];
    }

    public function profile() {

        $profiles = array();

        $query = "SELECT * FROM `photographs`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $profiles[] = $row;
        }
        return $profiles;
    }

    public function store() {

        if (!file_exists("upload")) {
            mkdir("upload");
        }
        $ext = strtolower(pathinfo($this->profile_pic["name"], PATHINFO_EXTENSION));
        $file_name = basename(strtolower(substr($this->profile_pic["name"], 0, 15)));
        $path = trim($file_name);
        if (!file_exists("upload/" . $path)) {
            move_uploaded_file($this->profile_pic["tmp_name"], "upload/" . $path);
        }

        $res = mysql_query("SELECT * FROM photographs");
        $num_rows = mysql_num_rows($res);

        if ($num_rows >= 1) {
            Utility::redirect('edit.php');
        } else {
          
            $query = "INSERT INTO `resume`.`photographs` (`users_id`,`profile_pic`) VALUES ('" . $this->user_id . "','" . $path . "')";
            $result = mysql_query($query);

            if ($result) {
                Utility::message("<div class=\"message_success\">Profile picture has added successfully.</div>");
            } else {
                Utility::message("<div class=\"message_error\">There is an error while added profile picture. Please try again later.</div>");
            }
            Utility::redirect('profile.php');
        }
    }

    public function show($id = false) {

        $query = "SELECT * FROM `photographs` WHERE id=" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_object($result);
        return $row;
    }

    public function update() {

        if (!file_exists("upload")) {
            mkdir("upload");
        }
        $ext = strtolower(pathinfo($this->profile_pic["name"], PATHINFO_EXTENSION));
        $file_name = basename(strtolower(substr($this->profile_pic["name"], 0, 15)));
        $path = trim($file_name);
        if (!file_exists("upload/" . $path)) {
            move_uploaded_file($this->profile_pic["tmp_name"], "upload/" . $path);
        }
        $query = "UPDATE `resume`.`photographs` SET ";
        if ($path && !empty($this->profile_pic["name"])) {
            $query .="`profile_pic`='{$path}' ";
        }
        $query .= "WHERE `photographs`.`id` =".$this->user_id;

        $result = mysql_query($query);

        if ($result) {
            Utility::message("<div class=\"message_success\">Profile Picture is edited successfully.</div>");
        } else {
            Utility::message("<div class=\"message_error\">There is an error while saving data. Please try again later.</div>");
        }

        Utility::redirect('profile.php');
    }

    public function delete($id = null) {
   
        if (is_null($id)) {
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('profile.php');
        }

        $query = "DELETE FROM `photographs` WHERE `photographs`.`id` = " . $id;
        $result = mysql_query($query);

        if ($result) {
         
            Utility::message("<div class=\"message_success\">Profile Picture is deleted successfully.</div>");
        } else {
         
            Utility::message("<div class=\"message_error\">Cannot delete.</div>");
        }
        
      Utility::redirect('profile.php');
    }

}
