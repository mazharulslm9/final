<?php

namespace App\User;
use App\Database\Database;
use \PDO;
Class User {
    
    private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
    
    public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function register($uname,$upass,$umail)
	{
		try
		{
			$new_password = md5($upass);
			
			$stmt = $this->conn->prepare("INSERT INTO users(user_name,password,email) 
		                                               VALUES(:uname, :upass, :umail)");
												  
			$stmt->bindparam(":uname", $uname);
                        $stmt->bindparam(":upass", $new_password);
			$stmt->bindparam(":umail", $umail);
													  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}
	
	
	public function doLogin($uname,$umail,$upass)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT id, user_name, password, email FROM users WHERE user_name=:uname OR email=:umail ");
			$stmt->execute(array(':uname'=>$uname, ':umail'=>$umail));
                        
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
                        
                        
                        
			if($stmt->rowCount() == 1)
			{
                               // var_dump($upass);die();
                                $p = md5($upass);
				if($p == $userRow['password'])
				{       
					$_SESSION['user_session'] = $userRow['id'];
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function is_loggedin()
	{
		if(isset($_SESSION['user_session']))
		{
			return true;
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function doLogout()
	{
		session_destroy();
		unset($_SESSION['user_session']);
		return true;
	}
    
}